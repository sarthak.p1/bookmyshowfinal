import { Background, BookingDetails } from "hyperverge-sde-ui-library";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

function BookingDetail() {
  const navigate = useNavigate();
  const show = useSelector((state) => state.show.selectedShow);
  console.log(show);
  const time = show.startTimeTimestamp;
  const movieName = show.movie?.name;
  const seatMatrix = show.seatMatrix;
  //   const movieName = show.movie;
  //   const seatMatrix = show.seatMatrix;
  //   const date = show.startTimeTimestamp;
  const seatsSelected = [];
  seatMatrix.forEach((row, rowind) => {
    row.forEach((seatValue, colind) => {
      if (seatValue == 1) {
        seatsSelected.push((rowind * 10 + colind + 1).toString());
      }
    });
  });

  return (
    <>
      <Background
        className="z-[-1]"
        style={{ position: "fixed", width: "100%", height: "100%" }}
      />
      <div className="flex felx-row justify-center py-[10rem]">
        <BookingDetails
          date={time}
          title={movieName}
          tickets={seatsSelected}
          onCheckout={() => {
            navigate("/otp");
          }}
        />
      </div>
    </>
  );
}

export default BookingDetail;
