import { Background, PaymentSuccess } from "hyperverge-sde-ui-library";
import { useNavigate } from "react-router-dom";

function Payment() {
  const navigate = useNavigate();
  return (
    <>
      <Background
        className="z-[-1]"
        style={{ position: "fixed", width: "100%", height: "100%" }}
      />
      <div className="h-screen flex flex-col justify-center items-center">
        <PaymentSuccess
          onViewTicket={() => {
            navigate("/ticket-details");
          }}
          onBackToHomePage={() => {
            navigate("/nowshow");
          }}
        />
      </div>
    </>
  );
}

export default Payment;
