import { SeatGrid, Background, CostPreview } from "hyperverge-sde-ui-library";
import { useParams } from "react-router-dom";
import {
  selectedShow,
  unsubscribeShowSeats,
  selectedSeat,
  deselectedSeat,
} from "../feature/showSlice";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { registerShowSocket } from "../utils/registerShowSocket";
import { useNavigate } from "react-router-dom";

function Seats() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();
  console.log(id);

  useEffect(() => {
    dispatch(selectedShow(id));
  }, [dispatch, id]);

  useEffect(registerShowSocket(id, dispatch), []);

  const show = useSelector((state) => state.show.selectedShow);
  console.log(show);
  const movie_id = show.movieId;
  const seatMatrix = show.seatMatrix;
  // console.log(seatMatrix);
  // const seatMatrix = selectedShow.;

  const seatsSelected = [];
  seatMatrix.forEach((row, rowind) => {
    row.forEach((seatValue, colind) => {
      if (seatValue == 1) {
        seatsSelected.push((rowind * 10 + colind + 1).toString());
      }
    });
  });

  return (
    <div>
      <div className="h-dvh w-full flex flex-col justify-between">
        <div className="text-white text-4xl font-semibold font-['Poppins'] px-10 py-10">
          Seat
        </div>
        <Background
          className="z-[-1]"
          style={{ position: "fixed", width: "100%", height: "100%" }}
        />
        <div className="flex felx-row justify-center">
          <SeatGrid
            seatArray={seatMatrix}
            onSeatClick={(rowind, colind) => {
              if (seatMatrix[rowind][colind] == 0) {
                dispatch(
                  selectedSeat({
                    showId: id,
                    seatNumber: rowind * 10 + colind + 1,
                  })
                );
              } else if (seatMatrix[rowind][colind] == 1) {
                dispatch(
                  deselectedSeat({
                    showId: id,
                    seatNumber: rowind * 10 + colind + 1,
                  })
                );
              }
            }}
          />
        </div>

        <div>
          <CostPreview
            seats={seatsSelected}
            onClickBack={() => {
              navigate(`/movies/${movie_id}`);
            }}
            onClickProceed={() => {
              navigate("/bookingDetails");
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default Seats;
