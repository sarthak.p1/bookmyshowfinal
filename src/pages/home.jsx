import React from "react";
import {
  Background,
  LeftRegister,
  RightRegister,
} from "hyperverge-sde-ui-library";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { getAuthToken, signIn, signOut } from "../utils/accountService";

function HomePage() {
  useEffect(() => {
    const user = getAuthToken();
    if (user) {
      navigate("/nowshow");
    }
  });
  const navigate = useNavigate();
  return (
    <div className="flex flex-row h-screen">
      <div className="w-1/2">
        <Background
          className="w-1/2 z-[-1]"
          style={{ position: "fixed", height: "100%" }}
        />
        <LeftRegister />
      </div>

      <div className="w-1/2">
        <RightRegister
          handleSubmit={() => signIn()}
          handleLogin={() => navigate("/login")}
        />
      </div>
    </div>
  );
}

export default HomePage;
