import {
  Background,
  Navbar,
  ShowsLeft,
  ShowsRight,
  TheatreList,
  DateList,
  TimeList,
  MovieDetails,
  ShowDetails,
} from "hyperverge-sde-ui-library";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import {
  selectedTheatreIndexInput,
  selectedDateIndexInput,
  selectedTimeIndexInput,
  selectedMovie,
} from "../feature/movieSlice";
import { useDispatch, useSelector } from "react-redux";
import { signOut } from "../utils/accountService";

function NowshowDescription() {
  // console.log(selectedMovie);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [date, setDate] = useState(false);
  const [time, setTime] = useState(false);
  const { id } = useParams();

  const selectedTheatreIndex = useSelector(
    (state) => state.movie.selectedMovie.selectedTheatreIndex
  );
  const listOfTheaters = useSelector(
    (state) => state.movie.selectedMovie.listOfTheatres
  );

  const listOfDate = useSelector(
    (state) => state.movie.selectedMovie.listOfDatesOfShowsOfSelectedTheatre
  );

  const indexOfDate = useSelector(
    (state) => state.movie.selectedMovie.selectedDateIndex
  );

  const listOfTime = useSelector(
    (state) =>
      state.movie.selectedMovie.listOfTimesOfShowsOfSelectedTheatreAndDate
  );

  const indexOfTime = useSelector(
    (state) => state.movie.selectedMovie.selectedTimeIndex
  );

  const selectedMovies = useSelector((state) => state.movie.selectedMovie);
  // console.log(selectedMovies);
  // console.log(selectedMovies.selectedShow?.id);
  const selectedShowId = selectedMovies.selectedShow?.id;

  useEffect(() => {
    dispatch(selectedMovie(id));
  }, [dispatch]);
  return (
    <>
      <Background
        className="z-[-1]"
        style={{ position: "fixed", width: "100%", height: "100%" }}
      />
      <Navbar
        isLoggedIn={true}
        hideMyTickets={true}
        onLogout={() => signOut()}
      />
      <div
        // style={{ display: "grid", gridTemplateColumns: "1fr 1fr" }}
        // className="py-10 pr-[7rem] sm:flex sm:flex-col sm:grid-cols-1"
        className="py-10 pr-[7rem] 2xl:grid 2xl:grid-cols-2  sm:grid sm:grid-cols-1"
      >
        <ShowsLeft>
          <TheatreList
            theatres={listOfTheaters}
            selectedTheatreIndex={selectedTheatreIndex}
            onClick={(index) => dispatch(selectedTheatreIndexInput(index))}
          />
          <DateList
            dates={listOfDate}
            selectedDateIndex={indexOfDate}
            onClick={(index) => dispatch(selectedDateIndexInput(index))}
          />
          <TimeList
            times={listOfTime}
            selectedTimeIndex={indexOfTime}
            onClick={(index) => dispatch(selectedTimeIndexInput(index))}
          />
        </ShowsLeft>
        <ShowsRight>
          <MovieDetails
            name={selectedMovies.name}
            description={selectedMovies.description}
            imageUrl={selectedMovies.imageUrl}
            duration={`${selectedMovies.durationInHours}h ${selectedMovies.durationInMinutes}m`}
            type="Action"
          />
          <ShowDetails
            name={selectedMovies.selectedShow?.theatre.name}
            showStartTimeTimestamp={
              selectedMovies.selectedShow?.startTimeTimestamp
            }
            onProceed={() => {
              navigate(`/seats/${selectedShowId}`);
            }}
          />
        </ShowsRight>
      </div>
    </>
  );
}

export default NowshowDescription;
