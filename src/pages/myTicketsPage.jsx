import {
  Background,
  Navbar,
  Tabs,
  ViewTicket,
  ViewTicketList,
} from "hyperverge-sde-ui-library";
import { fetchAllTickets } from "../feature/ticketSlice";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { signOut } from "../utils/accountService";

function MyTickets() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  // console.log(tickets.pastTickets);
  // console.log(pastTickets);

  const ticket = useSelector((state) => state.ticket);
  console.log(ticket);
  const pastTickets = ticket.pastTickets;
  const UpcomingTickets = ticket.upcomingTickets;
  console.log(UpcomingTickets);
  UpcomingTickets.map(
    (tickets) => (
      console.log(tickets?.show?.startTimeTimestamp),
      console.log(tickets?.show?.movie?.name),
      console.log(tickets?.bookedSeats)
    )
  );

  useEffect(() => {
    dispatch(fetchAllTickets());
  }, []);

  return (
    <>
      <Background
        className="z-[-1]"
        style={{ position: "fixed", width: "100%", height: "100%" }}
      />
      <Navbar
        isLoggedIn={true}
        highlightMyTickets={true}
        onLogout={() => signOut()}
      />
      <Tabs tabLables={["Upcoming", "History"]}>
        <ViewTicketList>
          {UpcomingTickets &&
            UpcomingTickets.map((tickets) => (
              <ViewTicket
                date={tickets?.show?.startTimeTimestamp}
                title={tickets?.show?.movie?.name}
                tickets={tickets?.bookedSeats}
              />
            ))}
        </ViewTicketList>
        <ViewTicketList>
          {pastTickets &&
            pastTickets.map((tickets) => (
              <ViewTicket
                date={tickets?.show?.startTimeTimestamp}
                title={tickets?.show?.movie?.name}
                tickets={tickets?.bookedSeats}
              />
            ))}
        </ViewTicketList>
      </Tabs>
    </>
  );
}

export default MyTickets;
