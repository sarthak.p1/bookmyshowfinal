import {
  Background,
  Navbar,
  Card,
  CardList,
  NowShowing,
  Button,
} from "hyperverge-sde-ui-library";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { fetchRecommendedMovies } from "../feature/movieSlice";
import { getAuthToken, signIn, signOut } from "../utils/accountService";

import { useEffect } from "react";

function NowShow() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchRecommendedMovies());
  }, [dispatch]);
  const movies = useSelector((state) => state.movie.recommendedMovies);
  console.log(movies, "react redux");
  return (
    <div>
      <Background
        className="z-[-1]"
        style={{ position: "fixed", width: "100%", height: "100%" }}
      />
      <div>
        <Navbar
          isLoggedIn={true}
          onMyTickets={() => navigate("/myTickets")}
          onLogout={() => signOut()}
        />
      </div>

      <div className="font-primary">
        <NowShowing />
      </div>
      <CardList>
        {movies.map((movie) => (
          <Card
            cardName={movie.name}
            imageUrl={movie.imageUrl}
            imageAltText={movie.alt}
            onClick={() => {
              navigate(`/movies/${movie.id}`);
            }}
          />
        ))}
      </CardList>
    </div>
  );
}

export default NowShow;
