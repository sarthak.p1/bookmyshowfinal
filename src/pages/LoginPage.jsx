import React from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
// import { getUserLoggedIn } from "../feature/ticketSlice";
import { getAuthToken, signIn, signOut } from "../utils/accountService";

import { Background, Login } from "hyperverge-sde-ui-library";
function LoginPage() {
  useEffect(() => {
    const user = getAuthToken();
    if (user) {
      navigate("/nowshow");
    }
  });
  const navigate = useNavigate();

  const handleSubmit = () => {
    // dispatch(getUserLoggedIn());
    signIn();
  };

  return (
    <div className="flex items-center justify-center min-h-screen bg-gray-200">
      <Background
        className="z-[-1]"
        style={{ position: "fixed", width: "100%", height: "100%" }}
      />
      <Login onHandleSubmit={handleSubmit} />
    </div>
  );
}

export default LoginPage;
