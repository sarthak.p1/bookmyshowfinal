import React from "react";
import axios from "axios";
import { Background, OTPInput } from "hyperverge-sde-ui-library";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

function Otp() {
  const navigate = useNavigate();

  const show = useSelector((state) => state.show.selectedShow);
  console.log(show);
  const showId = show.id;
  console.log(showId);
  const seatMatrix = show.seatMatrix;
  // console.log(seatMatrix);
  // const seatMatrix = selectedShow.;

  const seatNumbers = [];
  seatMatrix.forEach((row, rowind) => {
    row.forEach((seatValue, colind) => {
      if (seatValue == 1) {
        seatNumbers.push(rowind * 10 + colind + 1);
      }
    });
  });
  return (
    <div className="h-screen flex flex-col justify-center items-center">
      <Background
        className="z-[-1]"
        style={{ position: "fixed", width: "100%", height: "100%" }}
      />
      <OTPInput
        correctOTP={"1234"}
        handleSubmit={async () => {
          await axios.post("http://localhost:3000/api/booking/book", {
            showId,
            seatNumbers,
          });
          navigate("/successfulPayment");
        }}
      />
    </div>
  );
}

export default Otp;
