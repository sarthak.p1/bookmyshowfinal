import { Background, Button, ViewTicket } from "hyperverge-sde-ui-library";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

function TicketDetails() {
  const navigate = useNavigate();
  const show = useSelector((state) => state.show.selectedShow);
  console.log(show);
  const time = show.startTimeTimestamp;
  const movieName = show.movie?.name;
  const seatMatrix = show.seatMatrix;
  console.log(show.movie?.name);
  console.log(show.seatMatrix);
  console.log(show.startTimeTimestamp);
  const seatsSelected = [];
  seatMatrix.forEach((row, rowind) => {
    row.forEach((seatValue, colind) => {
      if (seatValue == 1) {
        seatsSelected.push((rowind * 10 + colind + 1).toString());
      }
    });
  });

  return (
    <div className="w-full">
      <Background className="z-[-1]" />
      <div className="flex items-start pt-10 ">
        <h1 className="font-bold text-3xl h-40 w-50 text-white">
          Ticket Details
        </h1>
      </div>
      <div className="flex flex-col">
        <div className="flex items-center justify-center">
          <ViewTicket
            date={time}
            title={movieName}
            tickets={seatsSelected}
            onDownloadTicket={() => {
              console.log("check");
            }}
          />
        </div>
        <div className="flex items-center justify-center pt-48">
          <Button
            className="px-20 py-4 text-2xl font-normal"
            isSolid={false}
            disable={false}
            inactive={false}
            onClick={() => {
              navigate("/nowshow");
            }}
          >
            <p className=" font-primary">Back to Homepage</p>
          </Button>
        </div>
      </div>
    </div>
  );
}

export default TicketDetails;
