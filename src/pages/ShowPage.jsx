import React from "react";
import {
  Background,
  ShowsLeft,
  TheatreList,
  ShowsRight,
  DateList,
  TimeList,
  Button,
  Navbar,
  MovieDetails,
  ShowDetails,
} from "hyperverge-sde-ui-library";
import { Children } from "react";

function Show() {
  const d = new Date();
  return (
    <div>
      <Background
        className="z-[-1]"
        style={{ position: "fixed", width: "100%", height: "100%" }}
      />
      <div className="flex flex-row items-center justify-between">
        <div>
          <Navbar />
        </div>
        <div className="flex h-[2rem]">
          <Button
            className="bg-[#FF0000]"
            isSolid={true}
            disable={false}
            inactive={false}
          >
            LogOut
          </Button>
        </div>
      </div>
      <div className="flex flex-row items-center justify-between pr-28">
        <div>
          <ShowsLeft>
            <TheatreList></TheatreList>
            <DateList>
              <Button isSolid={false} disable={false} inactive={false}></Button>
            </DateList>
            <TimeList />
          </ShowsLeft>
        </div>
        <div>
          <ShowsRight>
            <MovieDetails
              movieImg="https://upload.wikimedia.org/wikipedia/en/thumb/b/bb/Hanu_Man_film_Release_poster.jpeg/220px-Hanu_Man_film_Release_poster.jpeg"
              movieName="Hanuman"
              movieDiscription="HanuMan', a superhero film by Prasthanth Varma and Teja Sajja, became 2024's first major hit. The film, about a man who discovers his superpowers,"
              movieDuration="2h 20m"
              movieType="Action"
            />
            <ShowDetails
              name="PVR Punjaguta"
              showtimeTimestamp={d}
              onClick={() => {
                console.log("show details");
              }}
            />
          </ShowsRight>
        </div>
      </div>
    </div>
  );
}

export default Show;
