// store.ts
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import movieReducer from "../feature/movieSlice";
import showReducer from "../feature/showSlice";
import ticketReducer from "../feature/ticketSlice";

const rootReducer = combineReducers({
    movie: movieReducer,
    show: showReducer,
    ticket: ticketReducer,
});
export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
});
