import io from "socket.io-client";
import { setPreviousSelectedSeat, updatePreviosSelectedSeat, } from "../feature/socketSlice";
export const socketMiddleware = (store) => (next) => (action) => {
    const { showId } = store.getState().socket;
    const socket = io("http://localhost:3000");
    socket.on("connect", () => {
        console.log("socket connected successfully");
    });
    socket.emit("viewing show", { show_id: showId }, (ack) => {
        console.log(ack);
    });
    socket.on("previously selected seats", (ack) => {
        console.log(ack);
        store.dispatch(setPreviousSelectedSeat({ previousSelectedSeat: ack }));
    });
    socket.on("selected seat", (data) => {
        console.log("Received show data:", data);
        store.dispatch(updatePreviosSelectedSeat({ previousSelectedSeat: data.seatNumber }));
    });
    //   if (action.payload.seatNumber) {
    //     socket.emit("selected seat", action.payload, (ack) => {
    //       console.log(ack);
    //     });
    //   }
    console.log(action);
    return next(action);
};
export default socketMiddleware;
