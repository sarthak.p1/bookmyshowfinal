import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { fetchRecommendedMovies } from "./feature/movieSlice";
import HomePage from "./pages/home";
import TicketDetails from "./pages/TicketDetailsPage";
import NowShow from "./pages/nowShow";
import Show from "./pages/ShowPage";
import Otp from "./pages/OtpPage";
import LoginPage from "./pages/LoginPage";
import Seats from "./pages/SeatsPage";
import NowshowDescription from "./pages/nowshowDescription";
import MyTickets from "./pages/myTicketsPage";
import BookingDetail from "./pages/BookingDetails";
import Payment from "./pages/SuccessfulPayment";
import { useDispatch } from "react-redux";
import { configureAxios } from "./axios-config";
import PrivateRoutes from "./utils/ProtectedRoutes";

import "./App.css";
configureAxios();

function App() {
  const dispatch = useDispatch();

  dispatch(fetchRecommendedMovies());

  return (
    <Router>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/" element={<PrivateRoutes />}>
          <Route path="/ticket-details" element={<TicketDetails />} />
          <Route path="/nowshow" element={<NowShow />} />
          <Route path="/movies/:id" element={<NowshowDescription />} />
          <Route path="/show" element={<Show />} />
          <Route path="/otp" element={<Otp />} />
          <Route path="/seats/:id" element={<Seats />} />
          <Route path="/myTickets" element={<MyTickets />} />
          <Route path="/bookingDetails" element={<BookingDetail />} />
          <Route path="/successfulPayment" element={<Payment />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
